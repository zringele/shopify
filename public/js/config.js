
document.addEventListener('DOMContentLoaded', function(){
    let ele = document.querySelector('#js-config');
    let postForm = function ( ev ) {
        ev.preventDefault();
        let data = {};
        let hasErrors = false;
        for (let i = 0; i < this.length; i++) {
            data[ this[i].id ] = this[i]
        }
        let payLoad = {
            'add_existing_user_abandoned': data['add_existing_user_abandoned'].checked,
            'add_not_existing_user_abandoned': data['add_not_existing_user_abandoned'].checked,
            'add_new_user': data['add_new_user'].checked,
            'existing_user_abandoned_mailinglist': data['existing_user_abandoned_mailinglist'].value,
            'not_existing_user_abandoned_mailinglist': data['not_existing_user_abandoned_mailinglist'].value,
            'new_user_mailinglist': data['new_user_mailinglist'].value,
        };

        let validateCheckboxAndSelect = function( checkbox, select) {
            if ( payLoad[checkbox] && payLoad[select] === "") {
                hasErrors = true;
                addClass(data[select], 'is-invalid');
            } else {
                removeClass(data[select], 'is-invalid');
            }
        };
        validateCheckboxAndSelect('add_existing_user_abandoned', 'existing_user_abandoned_mailinglist');
        validateCheckboxAndSelect('add_not_existing_user_abandoned', 'not_existing_user_abandoned_mailinglist');
        validateCheckboxAndSelect('add_new_user', 'new_user_mailinglist');

        if (!hasErrors) {
            sendPost('/api/settings', JSON.stringify( payLoad ) );

        }
    };
    ele.addEventListener("submit", postForm, false);

    let addClass = function(el, className){
        if (el.classList)
            el.classList.add(className);
        else
            el.className += ' ' + className;
    }

    let removeClass = function (el, className){
        if (el.classList)
            el.classList.remove(className);
        else
            el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
    }

    let sendPost = function( url, data ){
        let request = new XMLHttpRequest();
        request.open('POST', url, true);
        request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
        request.send( data );
    };
});
