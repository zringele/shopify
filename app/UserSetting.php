<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSetting extends Model
{
    protected $fillable = [
        'user_id', 'add_existing_user_abandoned', 'add_not_existing_user_abandoned',
        'add_new_user', 'existing_user_abandoned_mailinglist',
        'not_existing_user_abandoned_mailinglist', 'new_user_mailinglist'
    ];

}
