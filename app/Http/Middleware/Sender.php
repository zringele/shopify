<?php

namespace App\Http\Middleware;

use Closure;

class Sender
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user()->sender_api_key) {
            return redirect(route('sender'));
        }

        return $next($request);
    }
}
