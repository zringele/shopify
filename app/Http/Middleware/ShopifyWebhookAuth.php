<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Auth\AuthenticationException;

class ShopifyWebhookAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( $this->verifyWebHook( $request->getContent(), $request->header("x-shopify-hmac-sha256")) ) {
            return $next( $request );
        }
        throw new AuthenticationException();
    }

    private function verifyWebHook($data, $hmacHeader)
    {
        $calculatedHmac = base64_encode(hash_hmac('sha256', $data, env('SHOPIFY_SECRET'), true));
        return hash_equals($hmacHeader, $calculatedHmac);
    }
}
