<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\ShopifyApiHelper;
use App\Store;
use App\UserProvider;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Socialite;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginShopifyController extends Controller
{

    /**
     * @param Request $request
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function redirectToProvider(Request $request)
    {

        $this->validate($request, [
            'domain' => 'string|required'
        ]);


        $config = new \SocialiteProviders\Manager\Config(
            env('SHOPIFY_KEY'),
            env('SHOPIFY_SECRET'),
            env('SHOPIFY_REDIRECT'),
            ['subdomain' => $request->get('domain')]
        );

        return Socialite::driver('shopify')
            ->setConfig($config)
            ->scopes(['read_products', 'read_customers', 'read_orders', 'read_draft_orders', 'read_checkouts'])
            ->redirect();

    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {

        $shopifyUser = Socialite::driver('shopify')->stateless()->user();

        // Create user
        $user = User::firstOrCreate([
            'name' => $shopifyUser->name,
            'email' => $shopifyUser->email,
            'password' => '',
        ]);

//         Store the OAuth Identity
        $provider = UserProvider::updateOrCreate([
            'user_id' => $user->id,
            'provider' => 'shopify',
            'provider_user_id' => $shopifyUser->id,
            'provider_token' => $shopifyUser->token,
        ]);


        // Create shop
        $shop = Store::firstOrCreate([
            'name' => $shopifyUser->name,
            'domain' => $shopifyUser->nickname,
        ]);

        $shop->users()->syncWithoutDetaching([$user->id]);

        Auth::login($user, true);

        $shopifyApi = new ShopifyApiHelper( $provider, $shop );

        $shopifyApi->registerWebHooks( $provider, $shop );


        return redirect('/home/index');

    }

}
