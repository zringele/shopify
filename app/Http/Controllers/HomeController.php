<?php

namespace App\Http\Controllers;

use App\Helpers\SenderApiHelper;

use App\Helpers\ShopifyApiHelper;
use App\UserSetting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function userRedirected( )
    {
//        $user = Auth::user();
//        $shopifyApi = new ShopifyApiHelper( $user->providers()->first(), $user->stores()->first() );
//        dump( json_decode( $shopifyApi->getResource('admin/webhooks.json') ) ); die;
        $senderApi = new SenderApiHelper();
        $mailingLists =  $senderApi->getMailingLists();

        if ( is_object($mailingLists) && is_object($mailingLists->error)) {
            return view( 'error', ['message' => $mailingLists->error->message]);
        }
        return view( 'config' , ['mailinglists' => $mailingLists]);
    }

    public function setSettings( Request $request )
    {
        $settings = Auth::user()->settings()->firstOrNew( ['user_id' => Auth::user()->id] );
        $settings->fill( $request->all() );



        if ( $settings->save() )
            return 'success';
        return 'fail';

    }

    public function connectSender()
    {
        $callback = route('sender.callback', [ 'user_id' => Auth::user()->id, 'api_key' => 'API_KEY' ]);

        $href = env('SENDER_ROOT') . '/commerce/auth?return=' . urlencode( $callback );

        return view('connectsender', ['callback' => $href] );
    }

    public function senderCallback( Request $request )
    {
        $user = Auth::user();
        if ( (int) $request->user_id !== $user->id) {
            throw new UnauthorizedException();
        }
        $user->sender_api_key = $request->api_key;

        $user->save();

        return redirect('/home/index');
    }
}
