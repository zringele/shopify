<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Customer;
use App\Helpers\SenderApiHelper;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WebHookController extends Controller
{

    public function checkoutCreate(Request $request, $name )
    {
        $user = User::where('name', $name )->first();
        Auth::login($user);

        $settings =  $user->settings()->first();

        if(!$settings) {
            return;
        }

        $customer = $request->customer;

        $mailingList = $this->solveMailingList($settings, $customer);

        if ( $mailingList ) {
            $this->addToMailingList($customer, $mailingList);
        }

        $this->openOrUpdateCart( $request->cart_token,  $request->id, $request );

        return;
    }

    public function checkoutUpdate( Request $request, $name )
    {
        $user = User::where('name', $name )->first();

        Auth::login( $user );

        $settings =  $user->settings()->first();

        if(!$settings) {
            return;
        }

        $customer = $request->customer;

        $mailingList = $this->solveMailingList($settings, $customer);


        if ( $mailingList ) {
            $this->addToMailingList($customer, $mailingList);
        }

        $cart = Cart::where('cart_token', $request->cart_token)->first();

        if ( !$cart ) {
            $this->openOrUpdateCart( $request->cart_token,  $request->id, $request );
        } else {
            $this->openOrUpdateCart( $request->cart_token, null, $request );
        }

        if ($request->completed_at) {
            $this->closeCart($request->cart_token, 'cartConvert');
        }

    }

    public function cartUpdate( Request $request, $name )
    {
        $user = User::where('name', $name )->first();
        Auth::login( $user );

        $this->openOrUpdateCart( $request->token, null, $request );
    }

    public function customerCreate( Request $request, $name )
    {
        $user = User::where('name', $name )->first();
        Auth::login( $user );

        if ( !$request->accepts_marketing ) {
            return;
        }
        $settings = $user->settings()->first();

        if ( !$settings->add_new_user || $settings->new_user_mailinglist ) {
            return;
        }

        $this->addToMailingList( (array) $request, $settings->new_user_mailinglist);
    }

    public function deleteCustomer( Request $request )
    {
        Customer::where('email', $request->customer->email)->first()->delete();
        return;
    }

    public function customerData( Request $request )
    {
        return Customer::where('email', $request->customer->email)->first();
    }

    public function redactStore( Request $request )
    {

    }

    protected function addToMailingList($customer, $mailingList)
    {
        $senderApi = new SenderApiHelper();

        $response = $senderApi->addToMailingList( [
            'email' => $customer['email'],
            'firstname' => $customer['first_name'],
            'lastname' => $customer['last_name']
        ],
            $mailingList
        );

        if ( $response && $response->data && $response->data->saved > 0) {
            $addedrecipient = new Customer();

            $addedrecipient->email = $customer['email'];
            $addedrecipient->lastname = $customer['last_name'];
            $addedrecipient->firstname = $customer['first_name'];
            $addedrecipient->mailinglist = $mailingList;

            $addedrecipient->save();

            return true;
        }

        if (  $response && $response->data && $response->data->updated > 0 ) {
            Customer::updateOrCreate([
                'email' => $customer['email']
                ], [
                'email' => $customer['email'],
                'firstname' => $customer['first_name'],
                'lastname' => $customer['last_name'],
                'mailinglist' => $mailingList,
            ]);
        }
        return false;
    }

    protected function solveMailingList( $settings, $customer)
    {

        if( !$customer || !$customer['accepts_marketing'] ) {
            return false;
        }

        if ( $customer['state'] === 'enabled' ) {
            if ( !$settings->add_existing_user_abandoned ) {
                return false;
            }

            return $settings->existing_user_abandoned_mailinglist;

        } else {
            if ( !$settings->add_not_existing_user_abandoned ) {
                return false;
            }

            return $settings->not_existing_user_abandoned_mailinglist;
        }
    }

    protected function openOrUpdateCart( $cartToken, $checkoutId, $request )
    {
        $cart = Cart::firstOrCreate([ 'cart_token' => $cartToken ], [
            'status' => 1,
            'checkout_id' => $checkoutId
        ]);

        $this->cartTrack($request, $cart);

    }


    protected function closeCart($cartToken, $method )
    {
        $cart = Cart::where('cart_token', $cartToken)->first();

        if ( !$cart ) {
            return false;
        }

        $senderApi = new SenderApiHelper();

        $senderApi->{$method}( $cart->id );

        $cart->delete();

        return true;
    }

    /**
     * @param $request
     * @param Cart $cart
     */
    protected function cartTrack($request, Cart $cart): void
    {
        $products = [];

        foreach ($request->line_items as $key => $product) {
            $products[$key] = [
                'sku' => $product['sku'],
                'name' => $product['title'],
                'price' => $product['price'],
                'price_display' => $product['line_price'],
                'qty' => $product['quantity'],
                "image" => ""
            ];
        }

        $senderApi = new SenderApiHelper();

        $senderApi->cartTrack($request->email, $cart->id, $request->abandoned_checkout_url, $request->currency, $request->total_price, $products);
    }


}
