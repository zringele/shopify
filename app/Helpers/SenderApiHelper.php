<?php

namespace App\Helpers;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;

class SenderApiHelper
{
    /**
     * @var Client instance of GuzzleHttp\Client
     */
    protected $client;

    /**
     * @var object instance of App\UserProvider
     */
    protected $provider;

    /**
     * @var object instance of App\Store
     */
    protected $store;

    /**
     * @var string sender api key
     */
    protected $apiKey;

    /**
     * @var string base url for api requests
     */
    protected $url;

    /**
     * @var string token for authentification
     */
    protected $providerToken;
    /**
     * ShopifyApiHelper constructor.
     * @param $provider
     * @param $store
     */
    public function __construct( )
    {
        $this->client = new Client();

        $this->apiKey = Auth::user()->sender_api_key;
        $this->url = env('SENDER_API_ENDPOINT');
    }


    public function getMailingLists( )
    {
        $url = $this->url;

        $result = $this->client->post($url, [
            'form_params' => [
                'data' => json_encode([
                    'method' => 'listGetAllLists',
                    'params'  => [
                        'api_key' => $this->apiKey,
                    ]
                ])
            ]
        ]);

        return json_decode($result->getBody()->getContents());
    }

    public function addToMailingList( $email, $listId )
    {
        $url = $this->url;

        $result = $this->client->post($url, [
            'form_params' => [
                'data' => json_encode([
                    'method' => 'listSubscribe',
                    'params'  => [
                        'api_key' => $this->apiKey,
                        'list_id' => $listId,
                        'emails' => [ $email ]
                    ]
                ])
            ]
        ]);

        return json_decode($result->getBody()->getContents());
    }

    public function cartTrack( $email, $checkoutId, $recoveryUrl, $currency, $total, $products)
    {
        $url = env('SENDER_ECOMMERCE_URL') . 'cart_track';


        $result = $this->client->post($url, [
            'form_params' => [
                    'api_key' => $this->apiKey,
                    "email" => $email,
                    "external_id" => $checkoutId,
                    "url" => $recoveryUrl,
                    "currency" => $currency,
                    "grand_total" => $total,
                    "products" =>  $products,
        ]]);

        return $result->getBody();

    }

    public function cartDelete( $cartId )
    {
        $url = env('SENDER_ECOMMERCE_URL') . 'cart_delete';

        $result = $this->client->post($url, [
            'form_params' => [
                    'api_key' => $this->apiKey,
                    'external_id' => $cartId
            ]]);

        return $result->getBody();
    }

    public function cartConvert( $cartId )
    {
        $url = env('SENDER_ECOMMERCE_URL') . 'cart_convert';

        $result = $this->client->post($url, [
            'form_params' => [
                'api_key' => $this->apiKey,
                'external_id' => $cartId
            ]]);

        return $result->getBody();
    }
}
