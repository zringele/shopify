<?php

namespace App\Helpers;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\URL;

class ShopifyApiHelper
{
    /**
     * @var Client instance of GuzzleHttp\Client
     */
    protected  $client;

    /**
     * @var object instance of App\UserProvider
     */
    protected $provider;

    /**
     * @var object instance of App\Store
     */
    protected $store;

    /**
     * @var string name of user shop
     */
    protected $storeName;

    /**
     * @var string base url for api requests
     */
    protected $urlBase;

    /**
     * @var string token for authentification
     */
    protected $providerToken;
    /**
     * ShopifyApiHelper constructor.
     * @param $provider
     * @param $store
     */
    public function __construct( $provider, $store )
    {
        $this->client = new Client();

        $this->provider = $provider;
        $this->store = $store;

        $this->providerToken = $provider->provider_token;
        $this->storeName = $store->name;

        $this->urlBase = 'https://'.env('SHOPIFY_KEY').':'.env('SHOPIFY_SECRET').'@'.$store->name.'.myshopify.com/';
    }

    /**
     * Registers web hooks
     */
    public function registerWebHooks( )
    {
        $this->registerWebHook( 'checkouts/create', 'checkout.create' );

        $this->registerWebHook( 'checkouts/update', 'checkout.update' );

        $this->registerWebHook( 'carts/update', 'cart.update' );

        $this->registerWebHook('customers/create', 'customers.create');

        $this->registerWebHook('customers/update', 'customers.update');

        $this->registerWebHook('customers/delete', 'customers.delete');
    }

    /**
     * @param $topic
     * @param $endPoint
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function registerWebHook( $topic, $endPoint )
    {
        $route = route( $endPoint, [ 'shopName' => $this->storeName ] );

        $url = $this->urlBase.'admin/webhooks.json';

        $data = [
            "webhook"=> [
                "topic"=> $topic,
                "address"=> $route,
                "format"=> "json"
            ]
        ];

        $response = $this->client->post($url, [
            'form_params' => $data,
            'headers'  => [
                'X-Shopify-Access-Token' => $this->providerToken
            ]
        ]);

        return $response;
    }

    public function getResource( $resource )
    {
        $url = $this->urlBase.$resource;

        $result = $this->client->get($url, [
            'headers'  => [
                'X-Shopify-Access-Token' => $this->providerToken
            ]
        ]);

        return $result->getBody()->getContents();
    }

}
