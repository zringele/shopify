<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// WebHooks Start
Route::post('{shopName}/checkout/create', 'WebHookController@checkoutCreate')->name('checkout.create')->middleware('shopify.webhook');
Route::post('{shopName}/checkout/update', 'WebHookController@checkoutUpdate')->name('checkout.update')->middleware('shopify.webhook');

Route::post('{shopName}/cart/update', 'WebHookController@cartUpdate')->name('cart.update')->middleware('shopify.webhook');

Route::post('{shopName}/customers/create', 'WebHookController@customerCreate')->name('customers.create')->middleware('shopify.webhook');
Route::post('{shopName}/customers/update', 'WebHookController@customerUpdate')->name('customers.update')->middleware('shopify.webhook');
Route::post('{shopName}/customers/delete', 'WebHookController@customerDelete')->name('customers.delete')->middleware('shopify.webhook');

Route::post('/settings', 'HomeController@setSettings');


//WebHooks end
