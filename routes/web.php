<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/home/index', 'HomeController@userRedirected')->middleware( 'auth', 'sender');

Route::get('/', 'HomeController@index')->name('shopify');

Route::get('/sender', 'HomeController@connectSender')->name('sender');

Route::get('/sender/callback', 'HomeController@senderCallback')->name('sender.callback');

//Route::get('/carts/create', 'HomeController@index')->name('home');



Route::get('login/shopify', ['as' => 'login.shopify', 'uses' =>'Auth\LoginShopifyController@redirectToProvider']);
Route::get('login/shopify/callback', 'Auth\LoginShopifyController@handleProviderCallback');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
