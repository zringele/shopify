@extends('layouts/app')

@section('content')
    <div class="container">
    <form method="POST" action="#" id="js-config">
        <div class="form-row">
            <div class="col-sm-12 col-md-6">
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="add_new_user">
                    <label class="form-check-label" for="new-customer-checkbox">Add new user to mailinglist</label>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <select class="custom-select" id="new_user_mailinglist">
                    <option value="" disabled selected>Select mailinglist</option>
                    @foreach($mailinglists as $mailinglist)
                        <option value="{{$mailinglist->id}}">{{$mailinglist->title}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr class="col-xs-12">
        <div class="form-row">
            <div class="col-sm-12 col-md-6">
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="add_not_existing_user_abandoned">
                    <label class="form-check-label" for="abandoned-cart-unregistered-checkbox">Add unregistered user abandoned cart to mailinglist</label>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <select class="custom-select" id="not_existing_user_abandoned_mailinglist">
                    <option value="" disabled selected>Select mailinglist</option>
                    @foreach($mailinglists as $mailinglist)
                        <option value="{{$mailinglist->id}}">{{$mailinglist->title}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr class="col-xs-12">
        <div class="form-row">
            <div class="col-sm-12 col-md-6">
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="add_existing_user_abandoned">
                    <label class="form-check-label" for="abandoned-cart-registered-checkbox">Add registered user abandoned cart to mailinglist</label>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                    @if( count($mailinglists) )
                        <select class="custom-select" id="existing_user_abandoned_mailinglist">

                        <option value="" disabled selected>Select mailinglist</option>
                            @foreach($mailinglists as $mailinglist)
                                <option value="{{$mailinglist->id}}">{{$mailinglist->title}}</option>
                            @endforeach
                        </select>
                     @else
                       <span> No mailinglists found </span>
                    @endif
            </div>
        </div>
        <hr class="col-xs-12">
        <div class="form-row">
            <div class="col">
                <button type="submit" id="submit-config" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
    </div>
    <script type="text/javascript" src="{{ URL::asset('js/config.js') }}"></script>

@endsection
