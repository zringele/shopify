@extends('layouts/app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="text-center">
                        <h3>Connect to Sender</h3>
                    </div>
                    <hr class="mb-4">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2>
                                Thank you for choosing Sender.net`s Integration module!
                            </h2>
                            <p>
                                First you must authenticate yourself with sender.net,
                                click authenticate to enter your credentials
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <a role="button"  href="{{ $callback }}" class="btn btn-primary btn-block">Continue</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection