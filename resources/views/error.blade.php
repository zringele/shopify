@extends('layouts/app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card mb-3 border-danger text-danger">
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-xs-12 text-center">
                            <h2 class="text-center">
                                {{ $message }}
                            </h2>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection