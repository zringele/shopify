<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->index('idx_user');
            $table->string('new_user_mailinglist')->nullable();
            $table->string('existing_user_abandoned_mailinglist')->nullable();
            $table->string('not_existing_user_abandoned_mailinglist')->nullable();
            $table->boolean('add_new_user');
            $table->boolean('add_existing_user_abandoned');
            $table->boolean('add_not_existing_user_abandoned');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_settings');
    }
}
